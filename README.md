# 웹 페이지 프로젝트 진행

## 프로젝트 진행 과정

1.  Git을 이용해 프로젝트 환경을 설정합니다. GitLab이나 GitHub에서 프로젝트를 생성한 후 작업물을 공유할 것을 권장합니다. 
2.  기본적인 UI 구조를 짰다면 해당 스케치를 바탕으로 Atom, Visual Studio Code 등의 에디터를 이용해 HTML, CSS 언어로 문서를 작성합니다.
3.  HTML, CSS 작업이 끝났다면  `.html` 문서를 `.jsp` 문서로 옮긴 후,  [eclipse](https://www.eclipse.org/) 에디터를 사용해 jsp로 작업합니다.

## 프로젝트 공유

-   [Git](https://www.git-scm.com/)을 설치하여 [GitLab](https://gitlab.com/) 또는 [GitHub](https://github.com/) 등의 Git 관리 프로그램을 이용해 프로젝트를 공유합니다.
    -   Git을 사용할 경우 버전 관리를 할 수 있으므로 프로젝트 작업물 수정이 용이합니다.
    -   Git에 대해서 잘 모르겠다면 [Backlog](https://backlog.com/git-tutorial/kr/intro/intro1_1.html)를 참고해 주세요.
    -   [Git 사용 예제](https://gitlab.com/dlwndus0422/database-project)
-   Git은 원래 터미널을 이용해 조작하나, 터미널 사용이 익숙치 않은 사용자들을 위해 [Sourcetree](https://www.sourcetreeapp.com/) 프로그램을 이용할 수 있습니다. 이 프로그램은 Git 조작을 사용자가 편하게 할 수 있도록 도와줍니다.

#### Git 설치 방법

1.  [Git 설치 링크](https://git-scm.com/download/win)를 클릭하여 Git을 컴퓨터에 다운받고, 설치합니다.
2.  터미널에 접속하여  `git --version` 을 입력해보고, 정상적으로 숫자가 출력된다면 `git`이 설치된 것입니다.
3.  터미널에서 아래와 같이 `Git` 환경설정을 해줍니다.

```code
$ git config --global user.name {Git에서 사용하는 username}
$ git config --global user.email {Git에 로그인할 때 사용하는 이메일 주소}
```

## HTML, CSS 에디터

무료 에디터는 다음의 두 가지 프로그램을 추천합니다. 개인적으로 **Visual Studio Code**가 더 가벼우므로 추천합니다. Google에 [Atom 필수 플러그인](https://www.google.com/search?q=atom+%ED%95%84%EC%88%98+%ED%94%8C%EB%9F%AC%EA%B7%B8%EC%9D%B8&oq=atom+%ED%95%84%EC%88%98+%ED%94%8C%EB%9F%AC%EA%B7%B8%EC%9D%B8&aqs=chrome..69i57j69i64&sourceid=chrome&ie=UTF-8) 또는 [Visual Studio Code 확장 추천](https://www.google.com/search?ei=6RIuXMDFO5Xi-Aa7z52IDQ&q=Visual+Studio+Code+%ED%99%95%EC%9E%A5+%EC%B6%94%EC%B2%9C&oq=Visual+Studio+Code+%ED%99%95%EC%9E%A5+%EC%B6%94%EC%B2%9C&gs_l=psy-ab.3..0.17088.17088..17401...0.0..0.106.106.0j1......0....1j2..gws-wiz.d5evh-ghZ34) 등으로 검색하면 여러 가지 유용한 플러그인을 추천받을 수 있으므로 꼭 검색한 후 필요한 플러그인을 다운받아 사용하세요. 이 플러그인들은 코드를 제작하는 시간을 단축시켜 줍니다.

-   [Atom](https://atom.io/)

-   [Visual Studio Code](https://code.visualstudio.com/)

    | 확장                                        | 설명                                                         |
    | ------------------------------------------- | ------------------------------------------------------------ |
    | Active File In StatusBar                    | 현재 실행중인 파일의 경로를 하단 상태바에 표시합니다.        |
    | Angular 7 Snippets                          | HTML 태그 등을 입력할 때 자동 완성을 도와줍니다.             |
    | Auto Close Tag                              | HTML 태그를 자동으로 닫아줍니다.                             |
    | Auto Rename Tag                             | HTML 태그를 수정할 때, 자동으로 닫는 태그도 함께 수정해 줍니다. |
    | Beautify                                    | 들여쓰기, 개행 등을 이용해 코드를 더 보기 좋게 해줍니다.     |
    | Bracket Pair Colorizer                      | 내부 괄호마다 색상을 다르게 하여 알아보기 쉽게 해줍니다.     |
    | Color Highlight                             | 색상 코드를 입력하면 그 코드 위에 해당 색상을 표현해줍니다.  |
    | Git History                                 | 에디터 상에서 Git 이력을 바로 볼 수 있도록 해줍니다.         |
    | GitLens                                     | 에디터 상에서 Git add, commit 등을 바로 할 수 있도록 해줍니다. |
    | Highlight Matching Tag                      | 현재 마우스가 위치해 있는 요소의 여는 태그와 닫는 태그를 강조해 줍니다. |
    | HTML CSS Support                            | CSS에서 클래스, 아이디 선택자의 자동 완성을 도와줍니다.      |
    | indent-rainbow                              | 들여쓰기에 색상을 넣어 구분하기 쉽게 해줍니다.               |
    | Korean Language Pack for Visual Studio Code | Visual Studio Code의 언어 설정을 한글로 바꿔줍니다.          |
    | One Dark Pro                                | Visual Studio Code 테마입니다.                               |
    | Path Intellisense                           | 파일 경로 등을 입력해야 할 때 자동 완성을 도와줍니다.        |
    | vscode-icons                                | 파일의 확장자마자 아이콘을 다르게 하여 구분하기 쉽게 도와줍니다. |

## 참고 사이트

-   강의
    -   [생활코딩](https://opentutorials.org/course/3083)
    -   [edwith 웹 프로그래밍 강의](https://www.edwith.org/boostcourse-web)
    -   [webber study](http://webberstudy.com/)

-   개념
    -   [NTS UIT BLOG](http://wit.nts-corp.com/)
    -   [w3schools](https://www.w3schools.com/)
    -   [MDN web docs](https://developer.mozilla.org/ko/docs/Web)
